Furnarius Rufus PCI Milling Machine 
===================================

This repository contains the documentation for the Furnarius Rufus PCB Milling Machine (Fresadora PCB João-de-Barro in portuguese).

About the authors
-----------------

This project started by [Centro de Tecnologia Acadêmica](http://cta.if.ufrgs.br) of 
Instituto de Física / Universidade Federal do Rio Grande do Sul (IF/UFRGS).

Germano Postal - Main developer
Gabriel Krieger Nardon 
Rafael Peretti Pezzi

For more information, please visit the [Project website](http://cta.if.ufrgs.br/projects/fresadora-pci-joao-de-barro/wiki).

Licensing information
---------------------

Copyright 2014 by the Authors

This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2.

You may redistribute and modify this documentation under the terms of the
CERN OHL v.1.2. (http://ohwr.org/cernohl). This documentation is distributed
WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN OHL v.1.2 for applicable
conditions
